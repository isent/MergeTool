﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using MergeTool;
using MergeTool.Algorithms;
using System.Collections.Generic;
using MergeTool.Models;
using MergeTool.Enums;

namespace UnitTests
{
    [TestClass]
    public class TestBlindMerger
    {
        public bool DiffDetailsComparer(DiffDetails d1, DiffDetails d2)
        {
            return
                d1.StrNum == d2.StrNum &&
                d1.Author == d2.Author &&
                d1.Text == d2.Text &&
                d1.ConflictName == d2.ConflictName &&
                d1.IsResolved == d2.IsResolved;
        }

        public bool DiffListComparer(List<DiffDetails> l1, List<DiffDetails> l2)
        {
            if (l1.Count != l2.Count) return false;

            var res = true;
            var i = 0;

            while (res && i < l1.Count)
            {
                res &= DiffDetailsComparer(l1[i], l2[i]);
                i++;
            }

            return res;
        }

        [TestMethod]
        public void TrueIfDevsNoChanges()
        {
            var dev1 = new List<string> {
                "aaa"
            };

            var dev2 = new List<string> {
                "aaa"
            };

            var origin = new List<string> {
                "aaa"
            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Dev1, "aaa", null, true),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }

        [TestMethod]
        public void TrueIfDevsDidSameChanges()
        {
            var dev1 = new List<string> {
                "aaa"
            };

            var dev2 = new List<string> {
                "aaa"
            };

            var origin = new List<string> {
                "bbb"
            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Dev1, "aaa", null, true),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }

        [TestMethod]
        public void TrueIfDevsChangedSameStr()
        {
            var dev1 = new List<string> {
                "aaa"
            };

            var dev2 = new List<string> {
                "bbb"
            };

            var origin = new List<string> {
                "ccc"
            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Origin, "ccc", $"{Settings.NameConflict} 1", false),
                new DiffDetails(0, AuthorTypes.Dev1, "aaa", $"{Settings.NameConflict} 1", false),
                new DiffDetails(0, AuthorTypes.Dev2, "bbb", $"{Settings.NameConflict} 1", false),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }

        [TestMethod]
        public void TrueIfDev1Changed()
        {
            var dev1 = new List<string> {
                "bbb"
            };

            var dev2 = new List<string> {
                "aaa"
            };

            var origin = new List<string> {
                "aaa"
            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Dev1, "bbb", null, true),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }

        [TestMethod]
        public void TrueIfDev2Changed()
        {
            var dev1 = new List<string> {
                "aaa"
            };

            var dev2 = new List<string> {
                "bbb"
            };

            var origin = new List<string> {
                "aaa"
            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Dev2, "bbb", null, true),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }

        [TestMethod]
        public void TrueIfDevsChangedSameStrAndOriginHasNoStr()
        {
            var dev1 = new List<string> {
                "aaa",
            };

            var dev2 = new List<string> {
                "bbb",
            };

            var origin = new List<string> {

            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Dev1, "aaa", $"{Settings.NameConflict} 1", false),
                new DiffDetails(0, AuthorTypes.Dev2, "bbb", $"{Settings.NameConflict} 1", false),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }

        [TestMethod]
        public void TrueIfDev1HasLargerFile()
        {
            var dev1 = new List<string> {
                "bbb",
            };

            var dev2 = new List<string> {

            };

            var origin = new List<string>
            {
                "aaa"
            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Dev1, "bbb", null, true),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }

        [TestMethod]
        public void TrueIfDev2HasLargerFile()
        {
            var dev1 = new List<string> {
                
            };

            var dev2 = new List<string>
            {
                "bbb"
            };

            var origin = new List<string>
            {
                "aaa"
            };

            var merger = new BlindDiffChecker();
            var res = merger.FindDiff(dev1, dev2, origin);

            var diffs = new List<DiffDetails>
            {
                new DiffDetails(0, AuthorTypes.Dev2, "bbb", null, true),
            };

            Assert.IsTrue(DiffListComparer(res, diffs));
        }
    }
}
