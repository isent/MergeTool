﻿using MergeTool.Algorithms;
using MergeTool.Models;
using MergeTool.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MergeTool.FileWriters;

namespace MergeTool
{
    public class MergeManager
    {
        public IDiffAlgorithm DiffAlg { get; set; }

        public IUI UI { get; set; }

        public IMergeFileWriter FileWriter { get; set; }

        public MergeManager(IDiffAlgorithm diffAlg, IUI ui, IMergeFileWriter fw)
        {
            DiffAlg = diffAlg;
            UI = ui;
            FileWriter = fw;
        }

        public void Merge(List<string> dev1, List<string> dev2, List<string> origin)
        {
            var diffs = DiffAlg.FindDiff(dev1, dev2, origin);
            UI.DisplayMerge(diffs);
            FileWriter.WriteToFile(diffs);
        }
    }
}
