﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeTool.Models
{
    public class Author
    {
        public readonly string Name;

        public readonly ConsoleColor TextColor;

        public Author(string name, ConsoleColor textColor)
        {
            Name = name;
            TextColor = textColor;
        }
    }
}
