﻿using MergeTool.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeTool.Models
{
    public class DiffDetails
    {
        public int StrNum { get; set; }

        public AuthorTypes Author { get; set; }

        public string Text { get; set; }

        public string ConflictName { get; set; }

        public bool IsResolved { get; set; }

        public DiffDetails(int strNum, AuthorTypes author, string text, string conflictName, bool isResolved)
        {
            StrNum = strNum;
            Author = author;
            ConflictName = conflictName;
            IsResolved = isResolved;
            Text = text;
        }
    }
}
