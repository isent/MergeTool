﻿using MergeTool.Enums;
using MergeTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeTool
{
    //TODO: move to file
    public static class Settings
    {
        public const string NameDev1 = "dev 1";

        public const string NameDev2 = "dev 2";

        public const string NameOrigin = "orig ";

        public const string NameConflict = "Conflict";

        public const ConsoleColor ColorDev1 = ConsoleColor.Cyan;

        public const ConsoleColor ColorDev2 = ConsoleColor.Yellow;

        public const ConsoleColor ColorOrigin = ConsoleColor.White;

        public const ConsoleColor ColorConflictResolved = ConsoleColor.Black;

        public const ConsoleColor ColorConflictNotResolved = ConsoleColor.DarkRed;

        public const ConsoleColor ColorConflictText = ConsoleColor.Black;

        public const string FilePathDev1 = @"Files/Dev1.txt";

        public const string FilePathDev2 = @"Files/Dev2.txt";

        public const string FilePathOrigin = @"Files/Origin.txt";

        public const string FilePathResult = @"Files/MergeResult.txt";

        public static Dictionary<AuthorTypes, Author> AuthorData = new Dictionary<AuthorTypes, Author>
        {
            { AuthorTypes.Dev1, new Author(NameDev1, ColorDev1)},
            { AuthorTypes.Dev2, new Author(NameDev2, ColorDev2)},
            { AuthorTypes.Origin, new Author(NameOrigin, ColorOrigin)}
        };
    }
}
