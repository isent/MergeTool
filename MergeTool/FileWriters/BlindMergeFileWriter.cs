﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MergeTool.Models;
using System.IO;

namespace MergeTool.FileWriters
{
    /// <summary>
    /// Writes merge results to file
    /// </summary>
    public class BlindMergeFileWriter : IMergeFileWriter
    {
        public void WriteToFile(List<DiffDetails> mergeRes)
        {
            var lines = new string[mergeRes.Count];
            string prev = null;

            for (int i = 0; i < mergeRes.Count; i++)
            {
                var line = mergeRes[i];

                if (line.ConflictName != prev)
                {
                    if (line.ConflictName != null)
                    {
                        if (prev != null)
                        {
                            lines[i] = $"{prev}";
                        }

                        lines[i] += $"{line.ConflictName}{Environment.NewLine}";
                    }
                    else lines[i] = $"{prev}{Environment.NewLine}";
                }

                prev = line.ConflictName;

                lines[i] += ($"{line.StrNum} {Settings.AuthorData[line.Author].Name}: {line.Text}");
            }

            File.WriteAllLines(Settings.FilePathResult, lines);
        }
    }
}
