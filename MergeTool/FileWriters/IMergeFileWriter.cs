﻿using MergeTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeTool.FileWriters
{
    public interface IMergeFileWriter
    {
        void WriteToFile(List<DiffDetails> diffs);
    }
}
