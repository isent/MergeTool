﻿using MergeTool.Enums;
using MergeTool.UI;
using MergeTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MergeTool.Algorithms;
using System.IO;
using MergeTool.FileWriters;

namespace MergeTool
{
    class Program
    {
        static void Main(string[] args)
        {
            var origin = File.ReadLines(Settings.FilePathOrigin).ToList();
            var dev1 = File.ReadLines(Settings.FilePathDev1).ToList();
            var dev2 = File.ReadLines(Settings.FilePathDev2).ToList();

            var mergeMgr = new MergeManager(new BlindDiffChecker(), new UIConsole(), new BlindMergeFileWriter());
            mergeMgr.Merge(dev1, dev2, origin);
        }
    }
}
