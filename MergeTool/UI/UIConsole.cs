﻿using MergeTool.Enums;
using MergeTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeTool.UI
{
    /// <summary>
    /// Displays merge results in console
    /// </summary>
    public class UIConsole : IUI
    {
        public void DisplayMerge(List<DiffDetails> diffs)
        {
            string prev = null;

            foreach (var line in diffs)
            {
                if (line.ConflictName != prev)
                {
                    Console.BackgroundColor = Settings.ColorConflictNotResolved;
                    Console.ForegroundColor = Settings.ColorConflictText;

                    if (line.ConflictName != null)
                    {
                        if (prev != null) {
                            Console.WriteLine(prev);
                        }
                        
                        Console.WriteLine(line.ConflictName);
                    }      
                    else Console.WriteLine(prev);
                }

                prev = line.ConflictName;

                Console.ForegroundColor = Settings.AuthorData[line.Author].TextColor;
                Console.BackgroundColor = line.IsResolved ? Settings.ColorConflictResolved : Settings.ColorConflictNotResolved;
                Console.WriteLine($"{line.StrNum} {Settings.AuthorData[line.Author].Name}: {line.Text}");
            }

            Console.ReadKey();
        }
    }
}
