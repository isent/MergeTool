﻿using MergeTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeTool.Algorithms
{
    public interface IDiffAlgorithm
    {
        List<DiffDetails> FindDiff(List<string> dev1, List<string> dev2, List<string> origin);
    }
}
