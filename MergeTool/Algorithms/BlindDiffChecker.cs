﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MergeTool.Enums;
using MergeTool.Models;

namespace MergeTool.Algorithms
{
    /// <summary>
    /// Iterates over dev1's changes and compares only full strings. 
    /// Favors dev1's changes over origin.
    /// Favors dev1's changes over dev2's if both are equal.
    /// </summary>
    public class BlindDiffChecker : IDiffAlgorithm
    {
        public int ConflictCount { get; private set; }

        public List<DiffDetails> FindDiff(List<string> dev1, List<string> dev2, List<string> origin)
        {
            ConflictCount = 1;
            List<DiffDetails> DiffResult = new List<DiffDetails>();

            for (int i = 0; i < dev1.Count; i++)
            {
                var d1 = dev1[i].Trim();

                if (dev2.Count <= i)
                {
                    DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev1, dev1[i], null, true));
                    continue;
                }

                else if(origin.Count <= i)
                {
                    var d2 = dev2[i].Trim();

                    if (d1 == d2)
                    {
                        DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev1, dev1[i], null, true));
                    }
                    else
                    {
                        DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev1, dev1[i], $"{Settings.NameConflict} {ConflictCount}", false));
                        DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev2, dev2[i], $"{Settings.NameConflict} {ConflictCount}", false));
                        ConflictCount++;
                    }
                }
                else
                {
                    var d2 = dev2[i].Trim();
                    var o = origin[i].Trim();

                    if (d1 == d2)
                    {
                        DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev1, dev1[i], null, true));
                    }
                    else
                    {
                        if (d1 == o)
                        {
                            DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev2, dev2[i], null, true));
                        }
                        else
                        {
                            if (d2 == o)
                            {
                                DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev1, dev1[i], null, true));
                            }
                            else
                            {
                                DiffResult.Add(new DiffDetails(i, AuthorTypes.Origin, origin[i], $"{Settings.NameConflict} {ConflictCount}", false));
                                DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev1, dev1[i], $"{Settings.NameConflict} {ConflictCount}", false));
                                DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev2, dev2[i], $"{Settings.NameConflict} {ConflictCount}", false));
                                ConflictCount++;
                            }
                        }
                    }
                }
            }

            if (dev2.Count > dev1.Count)
            {
                for (int i = dev1.Count; i < dev2.Count; i++)
                {
                    DiffResult.Add(new DiffDetails(i, AuthorTypes.Dev2, dev2[i], null, true));
                }
            }

            return DiffResult;
        }
    }
}
